'use strict';
var util = require('util');
var path = require('path');
var spawn = require('child_process').spawn;
var yeoman = require('yeoman-generator');

var FabGenerator = module.exports = function FabGenerator(args, options, config) {
  yeoman.generators.Base.apply(this, arguments);

  // setup the test-framework property, Gruntfile template will need this
  this.testFramework = 'mocha';
  // resolved to mocha by default
  this.hookFor('mocha', { as: 'app' });

  this.indexFile = this.readFileAsString(path.join(this.sourceRoot(), 'default.hbs'));

  this.on('end', function () {
    this.installDependencies({ skipInstall: options['skip-install'] });
  });

  this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(FabGenerator, yeoman.generators.Base);

FabGenerator.prototype.askFor = function askFor() {
  var cb = this.async();

  // have Yeoman greet the user.
  console.log(this.yeoman);
  console.log('This generator scaffolds out a basic web project. \n\nIt\'s based on generator-webapp, but simplified and with some other useful stuff added in - Grunticon for all your SVG needs, and Assemble for building static HTML files from modular templates and data.');

  var prompts = [{
    name: 'projectName',
    message: 'Give your project a name:'
  },
  {
    type: 'checkbox',
    name: 'features',
    message: 'what do you want to include:',
    choices: [{
      name: 'head.js',
      value: 'headjs',
      checked: true
    },
    {
      name: 'Twitter bootstrap',
      value: 'bootstrap',
      checked: true
    }
    ]
  }];

  this.prompt(prompts, function (props) {
    this.projectName = props.projectName;
   
    function hasFeature(feat) { return props.features.indexOf(feat) !== -1; }
    this.includeHeadjs = hasFeature('headjs');
    this.includeBootstrap = hasFeature('bootstrap');

    cb();
  }.bind(this));
};

FabGenerator.prototype.gruntfile = function gruntfile() {
  this.template('Gruntfile.js');
};

FabGenerator.prototype.packageJSON = function packageJSON() {
  this.template('_package.json', 'package.json');
};

FabGenerator.prototype.git = function git() {
  this.copy('gitignore', '.gitignore');
  this.copy('gitattributes', '.gitattributes');
};

FabGenerator.prototype.bower = function bower() {
  this.copy('bowerrc', '.bowerrc');
  this.copy('_bower.json', 'bower.json');
};

FabGenerator.prototype.jshint = function jshint() {
  this.copy('jshintrc', '.jshintrc');
};

FabGenerator.prototype.editorConfig = function editorConfig() {
  this.copy('editorconfig', '.editorconfig');
};

///

FabGenerator.prototype.writeIndex = function writeIndex() {

  this.indexFile = this.readFileAsString(path.join(this.sourceRoot(), 'default.hbs'));
  this.indexFile = this.engine(this.indexFile, this);
  var frameworkSelected = "foundation";
 // if (frameworkSelected == 'foundation') {

    this.indexFile = this.appendScripts(this.indexFile, 'js/foundation.js', [
      "bower_components/respond/respond.min.js"
      ]);
  //}
};



//
FabGenerator.prototype.scaffolding = function scaffolding() {
  this.mkdir('src');
  this.mkdir('src/images');
  this.mkdir('src/images/svg-src');
  this.mkdir('src/behaviour');
  this.mkdir('src/presentation');
  this.mkdir('src/presentation/partials');
  this.mkdir('src/presentation/partials/global');

  this.mkdir('src/sass');
  this.mkdir('src/sass/partials');
  this.mkdir('src/sass/partials/global');

  this.mkdir('src/templates');
  this.mkdir('src/templates/layouts');
  this.mkdir('src/templates/pages');
  this.mkdir('src/templates/partials');
  this.mkdir('src/templates/partials');

  this.copy('_!-edit-template-files-not-html', 'src/_!-edit-template-files-not-html');

  this.copy('default.hbs', 'src/templates/layouts/default.hbs');
  this.copy('header.hbs', 'src/templates/partials/header.hbs');
  this.copy('index.hbs', 'src/templates/pages/index.hbs');
  this.copy('patterns.hbs', 'src/templates/pages/patterns.hbs');

  this.copy('README.md','README.md');

  this.copy('example.json', 'src/example.json');

  this.copy('screen.scss', 'src/sass/screen.scss');
  this.copy('_all.scss', 'src/sass/partials/_all.scss');
  this.copy('_fontface.scss', 'src/sass/partials/global/_fontface.scss');
  this.copy('_general.scss', 'src/sass/partials/global/_general.scss');
  this.copy('_mixins.scss', 'src/sass/partials/global/_mixins.scss');
  this.copy('_normalize.scss', 'src/sass/partials/global/_normalize.scss');
  this.copy('_typography.scss', 'src/sass/partials/global/_typography.scss');
  this.copy('_variables.scss', 'src/sass/partials/global/_variables.scss');

  
  this.copy('ie7.scss', 'src/sass/ie7.scss');
  this.copy('ie8.scss', 'src/sass/ie8.scss');
  this.copy('ie9.scss', 'src/sass/ie9.scss');


  this.copy('engine.js', 'src/behaviour/main.js');
  this.copy('plugins.js', 'src/behaviour/plugins.js');

  this.copy('test.svg', 'src/images/svg-src/test.svg');
  this.copy('favicon.ico', 'src/favicon.ico');
  console.log("HEAD >>> " +this.includeHeadjs)
  console.log("BOOTSTRAP >>> " +this.includeBootstrap)
  if (this.includeHeadjs) {
    this.copy('head.js', 'src/behaviour/vendor/head.js');
  }
  if (this.includeBootstrap) {
      this.copy('bootstrap.scss', 'src/sass/bootstrap.scss');
      this.mkdir('src/sass/bootstrap/bootstrap/');      

      this.copy('bootstrap_sass/_alerts.scss' , 'src/sass/bootstrap/_alerts.scss');
      this.copy('bootstrap_sass/_badges.scss' , 'src/sass/bootstrap/_badges.scss');
      this.copy('bootstrap_sass/_breadcrumbs.scss' , 'src/sass/bootstrap/_breadcrumbs.scss');
      this.copy('bootstrap_sass/_button-groups.scss' , 'src/sass/bootstrap/_button-groups.scss');
      this.copy('bootstrap_sass/_buttons.scss' , 'src/sass/bootstrap/_buttons.scss');
      this.copy('bootstrap_sass/_carousel.scss' , 'src/sass/bootstrap/_carousel.scss');
      this.copy('bootstrap_sass/_close.scss' , 'src/sass/bootstrap/_close.scss');
      this.copy('bootstrap_sass/_code.scss' , 'src/sass/bootstrap/_code.scss');
      this.copy('bootstrap_sass/_component-animations.scss' , 'src/sass/bootstrap/_component-animations.scss');
      this.copy('bootstrap_sass/_dropdowns.scss' , 'src/sass/bootstrap/_dropdowns.scss');
      this.copy('bootstrap_sass/_forms.scss' , 'src/sass/bootstrap/_forms.scss');
      this.copy('bootstrap_sass/_glyphicons.scss' , 'src/sass/bootstrap/_glyphicons.scss');
      this.copy('bootstrap_sass/_grid.scss' , 'src/sass/bootstrap/_grid.scss');
      this.copy('bootstrap_sass/_input-groups.scss' , 'src/sass/bootstrap/_input-groups.scss');
      this.copy('bootstrap_sass/_jumbotron.scss' , 'src/sass/bootstrap/_jumbotron.scss');
      this.copy('bootstrap_sass/_labels.scss' , 'src/sass/bootstrap/_labels.scss');
      this.copy('bootstrap_sass/_list-group.scss' , 'src/sass/bootstrap/_list-group.scss');
      this.copy('bootstrap_sass/_media.scss' , 'src/sass/bootstrap/_media.scss');
      this.copy('bootstrap_sass/_mixins.scss' , 'src/sass/bootstrap/_mixins.scss');
      this.copy('bootstrap_sass/_modals.scss' , 'src/sass/bootstrap/_modals.scss');
      this.copy('bootstrap_sass/_navbar.scss' , 'src/sass/bootstrap/_navbar.scss');
      this.copy('bootstrap_sass/_navs.scss' , 'src/sass/bootstrap/_navs.scss');
      this.copy('bootstrap_sass/_normalize.scss' , 'src/sass/bootstrap/_normalize.scss');
      this.copy('bootstrap_sass/_pager.scss' , 'src/sass/bootstrap/_pager.scss');
      this.copy('bootstrap_sass/_pagination.scss' , 'src/sass/bootstrap/_pagination.scss');
      this.copy('bootstrap_sass/_panels.scss' , 'src/sass/bootstrap/_panels.scss');
      this.copy('bootstrap_sass/_popovers.scss' , 'src/sass/bootstrap/_popovers.scss');
      this.copy('bootstrap_sass/_print.scss' , 'src/sass/bootstrap/_print.scss');
      this.copy('bootstrap_sass/_progress-bars.scss' , 'src/sass/bootstrap/_progress-bars.scss');
      this.copy('bootstrap_sass/_responsive-utilities.scss' , 'src/sass/bootstrap/_responsive-utilities.scss');
      this.copy('bootstrap_sass/_scaffolding.scss' , 'src/sass/bootstrap/_scaffolding.scss');
      this.copy('bootstrap_sass/_tables.scss' , 'src/sass/bootstrap/_tables.scss');
      this.copy('bootstrap_sass/_theme.scss' , 'src/sass/bootstrap/_theme.scss');
      this.copy('bootstrap_sass/_thumbnails.scss' , 'src/sass/bootstrap/_thumbnails.scss');
      this.copy('bootstrap_sass/_tooltip.scss' , 'src/sass/bootstrap/_tooltip.scss');
      this.copy('bootstrap_sass/_type.scss' , 'src/sass/bootstrap/_type.scss');
      this.copy('bootstrap_sass/_utilities.scss' , 'src/sass/bootstrap/_utilities.scss');
      this.copy('bootstrap_sass/_variables.scss' , 'src/sass/bootstrap/_variables.scss');
      this.copy('bootstrap_sass/_wells.scss' , 'src/sass/bootstrap/_wells.scss');
                                                                                                    


      this.mkdir('src/behaviour/libs/bootstrap/');

      this.copy('bootstrap_js/affix.js', 'src/behaviour/libs/bootstrap/affix.js');
      this.copy('bootstrap_js/alert.js', 'src/behaviour/libs/bootstrap/alert.js');
      this.copy('bootstrap_js/button.js', 'src/behaviour/libs/bootstrap/button.js');
      this.copy('bootstrap_js/carousel.js', 'src/behaviour/libs/bootstrap/carousel.js');
      this.copy('bootstrap_js/collapse.js', 'src/behaviour/libs/bootstrap/collapse.js');
      this.copy('bootstrap_js/dropdown.js', 'src/behaviour/libs/bootstrap/dropdown.js');
      this.copy('bootstrap_js/modal.js', 'src/behaviour/libs/bootstrap/modal.js');
      this.copy('bootstrap_js/popover.js', 'src/behaviour/libs/bootstrap/popover.js');
      this.copy('bootstrap_js/tab.js', 'src/behaviour/libs/bootstrap/tab.js');
      this.copy('bootstrap_js/scrollspy.js', 'src/behaviour/libs/bootstrap/scrollspy.js');
      this.copy('bootstrap_js/tooltip.js', 'src/behaviour/libs/bootstrap/tooltip.js');
      this.copy('bootstrap_js/transition.js', 'src/behaviour/libs/bootstrap/transition.js');
  }
  // this.write('src/index.html', this.indexFile);
};




