/**
 * Authors (i.e. Large Egos ;):
 *  - Your name
*/
var Engine = {
    init: function () {
        Application.showAllContentWhenLoaded();
    },
    showAllContentWhenLoaded: function() {
        $('html').removeClass('visuallyhidden');
    }
}

$(document).ready(function () {
    Engine.init();
});
