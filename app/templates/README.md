html-boilerplate voor Fabrique
==============================

This is a basic boilerplate template setup and helper files to get frontend development going. It uses Django templating in combination with compass.

Requirements
------------

 * [Compass](http://compass-style.org/)
 * [Watchdog](https://pypi.python.org/pypi/watchdog)
 * [Django](http://www.djangoproject.org)
 * [Awesomeness](https://www.urbandictionary.com/define.php?term=awesomeness)

Optional
--------

 * [Virtualenvwrapper](https://bitbucket.org/dhellmann/virtualenvwrapper)

General
-------

To get this badboy on your development environment use:
```
    git clone https://github.com/fabrique/html-boilerplate myproject
```

The basic structure:
```
    plugins/                -- Vendor specific plugins (like bootstrap for example)
    sass/                   -- Sass files
    templates/              -- The static template directory
    templates/behaviour/    -- Behaviour stuff, JS, libs.. etc
    templates/presentation/ -- Presentation stuff, images, css... etc
    _base.html              -- The base HTML5 boilerplate template, you can derive the rest of your templates from this base.html
    config.rb               -- Sass configuration file
    DEPENDENCIES            -- Python dependencies; pip install -r DEPENDENCIES
    example.html            -- Example template
    example.json            -- Example JSON to seed the template if needed
    builder.py              -- Template development watchdog
    README.md               -- This awesome README
```

Default workflow
----------------

```
    $ git clone https://github.com/fabrique/html-boilerplate myproject
    $ cd myproject
    $ source virtualenvwrapper.sh
    $ mkvirtualenv myproject
    (myproject)$ pip install -r DEPENDENCIES
    (myproject)$ python builder.py
```

Template generation with builder.py
-----------------------------------

To start monitoring:
```
    $ python builder.py
```

You can also use Django fixtures by adding json files in the same filename. As per example:
```
    example.html
    example.json
```

This process writes the compiled templates to ./templates

The files starting with a underscore (_base.html) are ignored. You can use this notation when creating includes for example. But you can also just as easily create a "includes" directory, its up to you.

Conventions
-----------

 * Spaces not tabs.
 * 4 spaces per indent (also for the HTML, CSS and JS)
 * For vim users; make sure you have set noswapfile; set nobackup; set nowritebackup

Further
-------
 * [Fabrique](http://www.fabrique.nl)
 * [django-plango](https://github.com/fabrique/django-plango)
